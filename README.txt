THIS IS IS THE README FILE FOR ALEX'S DEFAULT GAME REPO

IT IS INTENDED AS AN EASY TO MODIFY DEFAULT PROJECT HERIARCHY FOR FUTURE GODOT 3.2 
PROJECTS AND POTENTIALLY BEYOND

If you are using this project, please keep your files organized the best you can.
Your future self and team members will thank you!

In this project one is intended to place useful code snippets, scripts and shaders
to allow for easy game creation in the future

Maintaining the code is your responsibility from hereon out to streamline development

If one wishes to save on space / avoid early confusion you can delete unnecessary code
and files but its not recommended because of dependencies

Good luck and keep on coding! :)

FYI #1: [textures] and [sprites] may appear to be the same folder group but textures 
are meant for 3D, backgrounds, or UI elements while sprites are intended for
2D animations, objects or characters!

When in doubt:
Big PNG / JPEG = texture
Small PNG / JPEG = sprite

FYI #2: [music] files should be stored as .wav or .ogg files since .mp3 is
proprietary (not supported by GODOT!)

*Godot has trouble with complex 3D files so find a way to mitigate that
*Ideally one should add redundancy in code so that if an asset DNE, the project will use
a default instead of crashing
