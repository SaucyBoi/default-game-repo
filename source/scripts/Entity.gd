extends "res://source/scripts/StateMachine.gd"

## this script is responsible for controlling all entity properties:
## the player, enemies, props and NPCS will use it

onready var globals = get_node("/root/Globals")
#onready var player_script = load("res://player/Player.gd").new()
onready var entity_parent = load("res://engine/Entity.gd")

const GRAVITY = -20
const GRAVITY2 = -32
const MAX_SLOPE_ANGLE = 40
const ARMOR_STRENGTH = 0.5

var vel = Vector3()
export (int) var hp = 0
export (int) var max_hp = 0
export (int) var armor = 0
export (int) var max_armor = 0
export (bool) var flying = false
var dead = false


## variables used for storing audio
export (String, "player_die_", "zombie_die_") var audio_die = "player_die_"
var num_audio_die = {}
export (String, "player_hurt_", "zombie_hurt_", "necromancer_hurt_") var audio_hurt = "player_hurt_"
var num_audio_hurt = {}

func _ready():
	for i in globals.audio_clips:
		if audio_die in globals.audio_clips[i]:
			#print(globals.audio_clips[i])
			num_audio_die.append[i]
		
		if audio_hurt in globals.audio_clips[i]:
			num_audio_hurt.append[i]


func _physics_process(_delta):
	if translation.y < -500:
		dead = true
	
	if !flying and !dead:
		if vel.y > 0:
			vel.y += _delta * GRAVITY
		else:
			vel.y += _delta * GRAVITY2
			
		vel = move_and_slide(vel, Vector3(0, 1, 0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))


## taking damage from a source
func take_damage(damage):
	## damage taken absorbed by armor
	hp -= (damage - round(damage * ARMOR_STRENGTH * (armor/max_armor)))
	hp = clamp(hp, 0, max_hp)
	
	## armor degrades because of damage
	armor -= round(ARMOR_STRENGTH * damage)
	armor = clamp(armor, 0, max_armor)
	globals.play_sound(audio_hurt, num_audio_hurt, false, "3D", null)
	
	if hp <= 0:
		## if dead do nothing and remove collision
		dead = true
		$CollisionShape.disabled = true
		globals.play_sound(audio_die, num_audio_die, false, "3D", null)


## health added from pickup
func add_health(additional_health):
	hp += additional_health
	hp = clamp(hp, 0, max_hp)


## armour added from pickup
func add_armor(additional_armor):
	armor += additional_armor
	armor = clamp(armor, 0, max_armor)


## randomly choses 0 or 1
func binary():
	return (randi() % 1)
