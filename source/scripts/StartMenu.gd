extends "res://source/scripts/MainMenu.gd"


func _on_Start_pressed():
	multi_thread.play_sound("menu_forward", 1, false, null, null)
	bg_loader.load_scene("res://source/engine/Clock.tscn")


func _on_Options_pressed():
	start.hide()
	options.show()


func _on_Quit_pressed():
	get_tree().quit()
