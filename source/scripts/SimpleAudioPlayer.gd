extends Node

var audio_node = null
var loop = false
onready var multi_thread = get_node("/root/MultiThread")
var dimension = null
var position = null

# Called when the node enters the scene tree for the first time.
func _ready():
	## sets 3D audio
	if dimension == "3D":
		audio_node = $AudioStreamPlayer3D
		
	## sets 2D audio
	elif dimension == "2D":
		audio_node = $AudioStreamPlayer2D
		
	## play sound regularly
	else:
		audio_node = $AudioStreamPlayer
	
	audio_node.connect("finished", self, "sound_finished")
	audio_node.stop()


func play_sound(audio_stream):
	## does nothing if sound does not exist
	if audio_stream == null:
		print ("No audio stream passed; cannot play sound")
		multi_thread.created_audio.remove(multi_thread.created_audio.find(self))
		queue_free()
		return
	
	audio_node.stream = audio_stream
	
	## 3D audio
	if audio_node is AudioStreamPlayer3D:
		if position != null:
			audio_node.global_transform.origin += position
	
	## 2D audio
	elif audio_node is AudioStreamPlayer2D:
		if position != null:
			audio_node.global_transform.origin += position
		
	## code for 1-dimensional audio not needed
	audio_node.play(0.0)


func sound_finished():
	if loop:
		audio_node.play(0.0)
		
	else:
		multi_thread.created_audio.remove(multi_thread.created_audio.find(self))
		audio_node.stop()
		queue_free()
