extends Node

var thread

## The purpose of this script is to generate a new cpu thread responsible for playing audio
const SIMPLE_AUDIO_PLAYER = preload("res://source/engine/SimpleAudioPlayer.tscn")

func _ready():
	thread = Thread.new()
	
	thread.start(self, "play_sound", "audio")

## a dict intended to store the game's sounds
var audio_clips = {}

var created_audio = []


func merge_dict(patch):
	for key in patch:
		audio_clips[key] = patch[key]


func play_sound(sound_name, sound_num, loop_sound = false, dimension = null, sound_position = null):
	if audio_clips.has(sound_name):
		var new_audio = SIMPLE_AUDIO_PLAYER.instance()
		new_audio.loop = loop_sound
		new_audio.dimension = dimension
		new_audio.position = sound_position
		
		add_child(new_audio)
		created_audio.append(new_audio)
		
		## if only one sound sample
		if sound_num == 1:
			new_audio.play_sound(audio_clips[sound_name])
		
		## if more than one sound sample randomize the sound
		elif sound_num > 1:
			new_audio.play_sound(audio_clips[sound_name + str((randi() % sound_num) + 1)])
		
		## otherwise throw an error message
		else:
			print("ERROR: cannot play sound that does not exist in audio_clips! SOUND_NAME = " + str(sound_name))
		
	else:
		print("ERROR: cannot play sound that does not exist in audio_clips! SOUND_NAME = " + str(sound_name))

func _exit_tree():
	thread.wait_to_finish()
