extends "res://source/scripts/MainMenu.gd"

onready var DEBUG_MENU = preload("res://source/menus/DebugMenu.tscn")


func _on_Debug_pressed():
	var clone = DEBUG_MENU.instance()
	var scene_root = get_tree().root.get_children()[0]
	scene_root.add_child(clone)


func _on_Video_pressed():
	pass # Replace with function body.


func _on_Audio_pressed():
	pass # Replace with function body.


func _on_Language_pressed():
	options.hide()
	language.show()


func _on_Back_pressed():
	options.hide()
	start.show()
