extends "res://source/scripts/MainMenu.gd"

## This script is responsible for changing language between the saved translations in the strings.csv file


func _on_English_pressed():
	TranslationServer.set_locale("en")


func _on_French_pressed():
	TranslationServer.set_locale("fr")


func _on_Spanish_pressed():
	TranslationServer.set_locale("es")


func _on_Back_pressed():
	language.hide()
	options.show()
