extends Button


func _ready():
	self.connect("hover", self, "mouse_entered")
	self.connect("pressed", self, "button_pressed")

func mouse_entered(hover):
	if hover:
		print("moused over")

func button_pressed(pressed):
	if pressed:
		print("button pressed")
