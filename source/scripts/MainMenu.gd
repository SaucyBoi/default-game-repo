extends Node


onready var globals = get_node("/root/Globals")
onready var screen = get_node("/root/Screen")
onready var multi_thread = get_node("/root/MultiThread")
onready var bg_loader = get_node("/root/BackgroundLoader")

onready var start = get_tree().get_root().find_node("StartMenu", true, false)
onready var options = get_tree().get_root().find_node("OptionsMenu", true, false)
onready var language = get_tree().get_root().find_node("OptionsLanguage", true, false)

var menu_audio = {
	"menu_forward": preload("res://assets/sounds/sf3-sfx-menu-select.wav"),
	"menu_back": preload("res://assets/sounds/sf3-sfx-menu-back.wav"),
	"menu_hover": preload("res://assets/sounds/coarse-click-minimal-ui-sounds.wav"),
	}

func _ready():
	start.show()
	options.hide()
	language.hide()
	
	multi_thread.merge_dict(menu_audio)
	multi_thread.play_sound("menu_forward", 1, null, null)
