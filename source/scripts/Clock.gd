extends Node2D
var hours = 0 
var minutes = 0
var seconds = 0

var font = null

var x0 = OS.get_real_window_size().x/2
var y0 = OS.get_real_window_size().y/2
var r = 220
var rs = 210
var rm = 175
var rh = 150
var c = ["XII", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI"]

func _ready():
	font = load("res://assets/fonts/UI_Regular.tres")
	set_process(true)
	VisualServer.set_default_clear_color(Color(0, 0, 0, 1))

func _process(_delta):
	time()
	update()

# determines OS time
func time():
	var timeDict = OS.get_time()
	hours = timeDict.hour
	minutes = timeDict.minute
	seconds = timeDict.second

func _draw():
	draw_circle(Vector2(x0, y0), r + 8, Color(1, 0.2, 0.2, 1))
	draw_circle(Vector2(x0, y0), r + 4, Color(0, 0, 0, 1))
	
	for s in range(0, 60, 1):
		#var ax = round(x0 - r * cos((PI/180) * (6 * s + 90)))
		#var ay = round(x0 - r * sin((PI/180) * (6 * s + 90)))
		
		
		var ax1 = round(x0 - (r - 5) * cos((PI/180) * (6 * s + 90)))
		var ay1 = round(y0 - (r - 5) * sin((PI/180) * (6 * s + 90)))
		
		# draws hour numbers to the screen
		if fmod(s, 5) == 0:
			draw_string(font, Vector2(ax1 - 8, ay1 + 6), String(c[s/5]), Color(1, 1, 1, 1), -1)
			
			# draws minute numbers to screen
		else:
			draw_line(Vector2(ax1, ay1), Vector2(ax1 + 1, ay1 + 1), Color(1, 1, 1, 1), 2)
			
	var xs = round(x0 - rs * cos((PI/180) * (6 * seconds + 90)))
	var ys = round(y0 - rs * sin((PI/180) * (6 * seconds + 90)))
	draw_line(Vector2(x0, y0), Vector2(xs, ys), Color(0.85, 0.65, 0.13, 1), 1)
	
	var xm = round(x0 - rm * cos((PI/180) * (6 * minutes + 90)))
	var ym = round(y0 - rm * sin((PI/180) * (6 * minutes + 90)))
	draw_line(Vector2(x0, y0), Vector2(xm, ym), Color(0.72, 0.53, 0.04, 1), 3)
	
	var xh = round(x0 - rh * cos((PI/180) * (90 + 6 * fmod(hours, 12) * 5 + minutes/12)))
	var yh = round(y0 - rh * sin((PI/180) * (90 + 6 * fmod(hours, 12) * 5 + minutes/12)))
	draw_line(Vector2(x0, y0), Vector2(xh, yh), Color(1, 0.55, 0, 1), 5)
