extends Control

var screen; var rec1; var rec2; var contain; var color = 0; var bak = []


func _ready():
	screen = [0, 0, get_viewport().size.x, get_viewport().size.y]
	contain = [screen]
	set_process_input(true)


func rect(c):
	bak.clear()
	for rec in c:
		randomize()
		if rec[2] > rec[1]:
			var w = round(rand_range(0.3, 0.7) * rec[2])
			rec1 = [rec[0], rec[1], w, rec[3]]
			rec2 = [rec[0] + w, rec[1], rec[2] - w, rec[3]]
			
		else:
			var h = round(rand_range(0.3, 0.7) * rec[3])
			rec1 = [rec[0], rec[1], rec[2], h]
			rec2 = [rec[0], rec[1] + h, rec[2], rec[3] - h]
			
		bak.append(rec1)
		bak.append(rec2)
		
	contain.clear()
	
	for r in bak:
		contain.append(r)
		print(contain)


func _input(_event):
	if Input.is_key_pressed(KEY_SPACE):
		rect(contain)
		update()


func draw():
	for r in contain:
		color = Color(rand_range(0.1, 0.8), rand_range(0.1, 0.8), rand_range(0.1, 0.8), 1)
		draw_rect(Rect2(r[0] + 1, r[1] + 1, r[2] - 2, r[3] - 2), color)
	
