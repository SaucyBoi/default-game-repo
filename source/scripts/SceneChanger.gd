extends CanvasLayer

signal scene_changed()

onready var anim_player = $AnimationPlayer
onready var color = $Control/ColorRect

## scene_change takes file as a string path of the scene one wishes to load
func change_scene(file, delay = 0.5):
	yield(get_tree().create_timer(delay), "timeout")
	anim_player.play("fade")
	yield(anim_player, "animation_finished")
	
	## change scene returns an error code if it was successful
	#assert(get_tree().change_scene(path) == OK)
	anim_player.play_backwards("fade")
	yield(anim_player, "animation_finished")
	emit_signal("scene_changed")
