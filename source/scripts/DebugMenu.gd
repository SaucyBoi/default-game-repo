extends Control

func _ready():
	$Ready.text = "OS: " + OS.get_name() + "\n" + "Godot Version: " + Engine.get_version_info()["string"] + "\n"

func _process(_delta):
	$Process.text = "FPS: " + str(Engine.get_frames_per_second())
