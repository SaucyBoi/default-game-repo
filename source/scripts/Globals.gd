extends Node

var fullscreen = OS.is_window_fullscreen()

func _process(_delta):
	if Input.is_action_pressed("ui_cancel"):
		get_tree().quit()
	
	if Input.is_action_just_pressed("fullscreen"):
		if fullscreen:
			Screen.set_windowed()
			fullscreen = false
			
		else:
			Screen.set_fullscreen()
			fullscreen = true
	
# Cheat code
var sequence = [
	KEY_UP,
	KEY_UP,
	KEY_DOWN,
	KEY_DOWN,
	KEY_LEFT,
	KEY_RIGHT,
	KEY_LEFT,
	KEY_RIGHT,
	KEY_B,
	KEY_A
]
var sequence_index = 0

func cheat_code(event):
	if event.type == InputEvent.KEY and event.pressed:
		if event.scancode == sequence[sequence_index]:
			sequence_index += 1
			if sequence_index == sequence.size():
				print("Konami code activated")
				sequence_index = 0
		else:
			sequence_index = 0
